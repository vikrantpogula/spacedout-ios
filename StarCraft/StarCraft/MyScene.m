//
//  MyScene.m
//  StarCraft
//
//  Created by Sumit Chaudhary on 15/04/14.
//  Copyright (c) 2014 starcraft. All rights reserved.
//

#import "MyScene.h"
#import <QuartzCore/QuartzCore.h>
@implementation MyScene

int noOfFrames;

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
      noOfFrames=0;
      [self createBackground];
      [self createDefenderAtPosition:CGPointMake(80, 150)];
      [self createDefenderAtPosition:CGPointMake(200, 250)];
      self.physicsWorld.gravity = CGVectorMake(0, 0);
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
}

-(void)update:(CFTimeInterval)currentTime{
  if (noOfFrames%120==0) {
    [self createAsteroidAtRandomPosition];
  }
  noOfFrames++;
}

#pragma mark - Node spawn methods


-(void)createDefenderAtPosition:(CGPoint)position{
  SKSpriteNode * defender = [SKSpriteNode spriteNodeWithImageNamed:@"defender"];
  defender.position = position;
  defender.size = CGSizeMake(80,80);
  defender.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:25];
  defender.name = @"defender";
  [self addChild:defender];
}

-(void)createAsteroidAtRandomPosition{
  SKSpriteNode * asteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid_blue"];
  asteroid.size = CGSizeMake(25, 50);
  asteroid.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(25, 50)];
  asteroid.name = @"asteroid";
  
  int xPos = arc4random()%320;
  asteroid.position = CGPointMake(xPos, 600);
  
  SKAction * moveAsteroid = [SKAction moveTo:CGPointMake(160, 0) duration:3];
  
  [asteroid runAction:moveAsteroid withKey:@"moveAsteroid"];
  [self addChild:asteroid];
}

-(void)createBackground{
  SKSpriteNode* background = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
  background.position = CGPointMake(160, 284);
  background.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:[UIScreen mainScreen].bounds];
  background.name = @"background";
  background.size = [UIScreen mainScreen].bounds.size;
  self.scaleMode = SKSceneScaleModeAspectFit;
  [self addChild:background];
}

@end
