//
//  AppDelegate.h
//  StarCraft
//
//  Created by Sumit Chaudhary on 15/04/14.
//  Copyright (c) 2014 starcraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
