//
//  main.m
//  StarCraft
//
//  Created by Sumit Chaudhary on 15/04/14.
//  Copyright (c) 2014 starcraft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
